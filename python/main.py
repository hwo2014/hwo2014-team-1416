﻿# Tuuhea Misaki360 rallibotti
# email: admin@tatatofly.org
# website: http://tatatofly.org
# By; Tatatofly

import json
import socket
import sys



class NoobBot(object):


    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

	# jannakakka.html
    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

	# Servulle paskeiden lahettamis paskeet
    def send(self, msg):
        self.socket.send(msg + "\n")

	# Joinimis paskeita [normit]
    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

	# Joinimis paskeita [custom raceihin]
    def	on_join_race(self, data=None):
        return self.msg("joinRace", {"botId": {"name": self.name,
                                               "key": self.key},
                                     "trackName": "germany", # Racen nimi, kommentoi pois jos ei tarvitse
                                     #"password": "schumi4ever", # Racen passu, kommentoi pois jos ei tarvitse
                                     "carCount": 3}) # Tan pitaa matsata racen arvoihin
	
    # Racen luomis paskeita [custom raceihin]
    def	on_create_race(self, data=None):
        return self.msg("createRace", {
                            "botId": {
                                "name": self.name,
                                "key": self.key},
                            "trackName": "germany", # Racen nimi, kommentoi pois jos ei tarvitse
                            #"password": "schumi4ever", # Racen passu, kommentoi pois jos ei tarvitse
                            "carCount": 1}) # Tan pitaa matsata racen arvoihin
								 
	# Lahinna ylimaaraista aloitus kikkaulua							 
    def on_mappia(self, data):
        rallit = data['race']
        rata = rallit['track']
        rataSessio = rallit['raceSession']
		
        pieceM = rata['pieces']
        lanesM = rata['lanes']
        global rataM
        rataM = len(pieceM)
		
        global kikikiMumM
        kikikiMumM = 1
		
        rataKPL = 0
        global kurvi
        kurvi = {}
		
        while rataM > rataKPL:
            pieceMM = pieceM[rataKPL]
            rataKPLS = str(rataKPL)
            rataKPLST = "kurvit"+rataKPLS
            if 'angle' in pieceMM:
                pieceAng = pieceMM['angle']
            else:
                pieceAng = 0
            kurvi[rataKPLST] = pieceAng
            
            rataKPL += 1
		
        rataNimi = rata['name']
        rataLaps = rataSessio['laps']
        rataMaxTime = rataSessio['maxLapTimeMs']
        rataAloitus = rata['startingPoint']
		
        rataAloitusPaikka = rataAloitus['position']
        rataAloitusKulma = rataAloitus['angle']
		
        rataAloitusPaikkaX = rataAloitusPaikka['x']
        rataAloitusPaikkaY = rataAloitusPaikka['y']
		
        rataMaxTimeM = rataMaxTime / 1000
		
		
        rataLapsS = str(rataLaps)
        rataMaxTimeT = str(rataMaxTimeM)
		
        
        print(" ")
        print("Rata: "+rataNimi)
        print("Kierrokset: "+rataLapsS)
        print("Max kierrosaika: "+rataMaxTimeT+"s")
        print(" ")
        	

	# Lahettaa kaasutukse servulle
    def throttle(self, throttle):
        self.msg("throttle", throttle)

	# Perus anaali
    def ping(self):
        self.msg("ping", {})

	# Kaynnistaa jonit ja msg loopit
    # Tassa myos muokataan keta rallia ajentaan: 
    # Muista conffata oikeat passut yms ylempana
    # self.join() On normi testi
    # self.on_create_race() On Racen luonti
    # self.on_join_race() On Raceen liittyminen
    def run(self):
        self.join() #korvataan taa
        self.msg_loop()

	# Servulle joinattud
    def on_join(self, data):
        print("Liitytty")
        self.ping()

	# Lahto laukaus
    def on_game_start(self, data):
        print("GO GO GO!!")
        #self.msg("switchLane", "Right")
        self.ping()
		

	# Auton driftaus kulmaan perustuva kaasun saato [nait kantsii muokkailla]
    def on_car_positions(self, data):
        kulma = data[0]
        ujo = kulma['angle']
        
        kiki = kulma['piecePosition']
        kikii = kiki['inPieceDistance']
        kikiki = kiki['pieceIndex']
        kikikii = str(kikiki)
        kikikiMum = kikiki + 1
        kikikiMumM = kikiki + 1
        if kikikiMum == rataM:
            kikikiMum = 0
        if kikikiMumM == rataM:
            kikikiMumM = 0
        kikikiiMum = str(kikikiMum)
        kikikiiMumM = str(kikikiMumM)
        kurvat = "kurvit"+kikikii
        kurvatMum = "kurvit"+kikikiiMum
        kurvatMumM = "kurvit"+kikikiiMumM
        datapaska = kurvi.__getitem__(kurvat)
        datapaskaMum = kurvi.__getitem__(kurvatMum)
        datapaskaMumM = kurvi.__getitem__(kurvatMumM)
		
        # Vauhti muuttujaa kantsii muutella 
		# Myos if ujo > x  on hyva tapa muokata vauhteja driftin mukaan
        # if datapaskan muuttaminen on lahes tarpeetonta
        # Ensimmainen if on jos suoralla ja edessa suora pala ja sen edessa     
        if datapaska < 5 and datapaska > -5 and datapaskaMum < 5 and datapaskaMum > -5 and datapaskaMumM < 5 and datapaskaMumM > -5:
            if ujo > 20:
                vauhti = 0.70 # Vitun tiukasta kurvista tuleva suora
            elif ujo > 1:
                vauhti = 0.70 # Kurvista tuleva pitka suora
            else:
                vauhti = 0.66 # Normi suora
		# elif jos tama pala suora ja seuraava muttei sita seuraava	
        elif datapaska < 5 and datapaska > -5 and datapaskaMum < 5 and datapaskaMum > -5:
            if ujo > 20:
                vauhti = 0.70 # Erittain epatodennakoinen muuttuja
            elif ujo > 1:
                vauhti = 0.70 # jos edellinen kurvi vain jatkui
            else:
                vauhti = 0.55 # Mutka lahenee
		# elif jos tama pala suora muttei seuraava		
        elif datapaska < 5 and datapaska > -5:
            if ujo > 20:
                vauhti = 0.30 # Epatodennakoinen muuttuja
            elif ujo > 1:
                vauhti = 0.30 # Tassa kaikki on kusemassa
            else:
                vauhti = 0.35 # Nailla mutkaan
        # elis jos mutkassa mutta seuraava ja sita seuraava suora
        elif datapaska > 5 or datapaska < -5 and datapaskaMum < 5 and datapaskaMum > -5 and datapaskaMumM < 5 and datapaskaMumM > -5:
            if ujo > 20:
                vauhti = 0.60 # Vitun tiukassa kurvissa ja seuraavat suoria
            elif ujo > 1:
                vauhti = 0.60 # Kurvissa ja tuleva pitka suora
            else:
                vauhti = 0.60 # Mutkassa ilman driftia seuraavat suoria
		# elif jos mutkassa mutta seuraava pala suora muttei enaa sita seuraava
        elif datapaska < 5 or datapaska > -5 and datapaskaMum < 5 and datapaskaMum > -5:
            if ujo > 20:
                vauhti = 0.20 # Vitun tiukassa kurvissa ja seuraava suora
            elif ujo > 1:
                vauhti = 0.20 # pien drift mutta seuraava suora
            else:
                vauhti = 0.30 # Mutkassa ilman driftia seuraava suora
        # muulloin eli kurvissa ja pelkkaa kurvia tulossa
        else:
            if ujo > 30:
                vauhti = 0.30 # Kurvissa vitulliset Driftaukset
            elif ujo > 1:
                vauhti = 0.30 # Kurvissa alkoi Driftaus
            else:
                vauhti = 0.30 # Kurvissa suoraan
        
        self.throttle(vauhti)

	# Mikali joku kilpailijoista ajaa ulos
    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

        
    # Kun ralli paattyy
    def on_game_end(self, data):
        print(" ")
        print("Maali!")
        self.ping()

	# Mikali jokin error niin printtaa sen
    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()	

	# Looppaa servulta tulevan tiedon hakua ja json parsetukset
    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'joinRace': self.on_join_race,
            'createRace': self.on_create_race,
            'gameStart': self.on_game_start,
            'gameInit':self.on_mappia,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()



# Hoitaa itse servulle connectaamisen

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Yhdistetaan parametreilla:")
        print("Hosti = {0}, Portti = {1}, Kuski = {2}, Avain = {3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        rataAloitusKulma = 0
        kikiki = 0
        bot.run()
